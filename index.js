	

	db.users.insertOne(
		{
			name: "single",
			accomodation: 2,
			price: "P1000.00",
			description: "A simple room with all the basic necessities",
			roomsAvailable: 10,
			isAvailable: false
		}
	);

	db.users.insertMany([
		{
			name: "double",
			accomodation: 3,
			price: "P2000.00",
			description: "A room fit for a small family going on a vacantion",
			roomsAvailable: 5,
			isAvailable: false
		},
		{
			name: "queen",
			accomodation: 4,
			price: "P4000.00",
			description: "A room with a queen sized bed perfect for a simple getaway",
			roomsAvailable: 15,
			isAvailable: false
		}
	]);


	db.users.find({name: "double"});
	

	db.users.updateOne(
		{	
			name: "queen"
		},
		{
			$set:{
				roomsAvailable: 0
			}
		}
	);

	db.users.deleteMany({roomsAvailable: 0});